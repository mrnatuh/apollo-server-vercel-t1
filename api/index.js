import { ApolloServer } from "apollo-server-express";
import { ApolloServerPluginCacheControl } from 'apollo-server-core';
import { ApolloServerPluginDrainHttpServer } from "apollo-server-core";
import http from "http";
import express from "express";
import cors from "cors";
import helmet from 'helmet';

import { typeDefs } from "../src/graphql/schema.js";
import { resolvers } from "../src/graphql/resolvers.js";

const app = express();

app.use(cors());
app.use(helmet());

app.use(express.json());

const httpServer = http.createServer(app);

const startApolloServer = async (app, httpServer) => {
    const server = new ApolloServer({
        typeDefs,
        resolvers,
        plugins: [
            ApolloServerPluginDrainHttpServer({ httpServer }),
            ApolloServerPluginCacheControl({ defaultMaxAge: 60 })
        ],
    });

    await server.start();

    server.applyMiddleware({ app });
};

startApolloServer(app, httpServer);

export default httpServer;