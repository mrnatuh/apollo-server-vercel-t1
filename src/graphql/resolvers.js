import { books } from '../data/books.js';

export const resolvers = {
    Query: {
        books: () => books,
        book: (_, { input }) => {
            const { title } = input;
            return books.filter(book => book.title.indexOf(title) > -1);
        }
    },
};