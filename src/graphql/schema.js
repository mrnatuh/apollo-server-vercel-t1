import { gql } from 'apollo-server-express';

export const typeDefs = gql`
    enum CacheControlScope {
        PUBLIC
        PRIVATE
    }

    directive @cacheControl(
        maxAge: Int
        scope: CacheControlScope
        inheritMaxAge: Boolean
    ) on FIELD_DEFINITION | OBJECT | INTERFACE | UNION

    type Book @cacheControl(maxAge: 120) {
        id: ID
        title: String
    }   

    input InputSearchBook { 
        title: String!
    }

    type Query {
        books(input: InputSearchBook): [Book]
        book(input: InputSearchBook): [Book]
    }
`;