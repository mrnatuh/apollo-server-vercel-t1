export const books = [
    {
        id: "f03c8893-3456-4977-b10f-a3234df5a343",
        title: 'Javascript for dummies'
    },
    {
        id: "7b9cb59c-bfbf-4ae3-b189-7096fe74b29d",
        title: 'Apollo for dummies'
    },
    {
        id: "b97c0b5e-96e8-4a96-8a0e-a9d90579fee5",
        title: 'CSS for dummies'
    },
    {
        id: "0421321d-13e1-4eed-8d35-2e8cba384aeb",
        title: 'HTML for dummies'
    },
    {
        id: "741210cf-70b5-4fd3-b304-307eba781a5f",
        title: 'Vue for dummies'
    },
    {
        id: "917fd882-c2be-42b7-85c2-a24f0e4ce041",
        title: 'PHP for dummies'
    },
    {
        id: "917fd882-c2be-42b7-85c2-a24f0e4ce041",
        title: 'React for dummies'
    }
];    